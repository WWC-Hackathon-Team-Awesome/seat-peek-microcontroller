/*
  Input Pull-up Serial

  This example demonstrates the use of pinMode(INPUT_PULLUP). It reads a digital
  input on pin 2 and prints the results to the Serial Monitor.

  The circuit:
  - momentary switch attached from pin 2 to ground
  - built-in LED on pin 13

  Unlike pinMode(INPUT), there is no pull-down resistor necessary. An internal
  20K-ohm resistor is pulled to 5V. This configuration causes the input to read
  HIGH when the switch is open, and LOW when it is closed.

  created 14 Mar 2012
  by Scott Fitzgerald

  This example code is in the public domain.

  http://www.arduino.cc/en/Tutorial/InputPullupSerial
*/
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <ESP8266HTTPClient.h>

ESP8266WiFiMulti WiFiMulti;
// constants won't change. They're used here to set pin numbers:
const int pin1 = 2;    // the number of the pushbutton pin
const int pin2 = 5;    // the number of the pushbutton pin
const int pin3 = 4;    // the number of the pushbutton pin
const int pin4 = 16;    // the number of the pushbutton pin

const int ledPin = LED_BUILTIN;      // the number of the LED pin

// Variables will change:
int buttonPushCounter1 = 0;   // counter for the number of button presses
int buttonState1 = 0;         // current state of the button
int lastButtonState1 = 0;     // previous state of the button

// Variables will change:
int buttonPushCounter2 = 0;   // counter for the number of button presses
int buttonState2 = 0;         // current state of the button
int lastButtonState2 = 0;     // previous state of the button

// Variables will change:
int buttonPushCounter3 = 0;   // counter for the number of button presses
int buttonState3 = 0;         // current state of the button
int lastButtonState3 = 0;     // previous state of the button

// Variables will change:
int buttonPushCounter4 = 0;   // counter for the number of button presses
int buttonState4 = 0;         // current state of the button
int lastButtonState4 = 0;     // previous state of the button

void setup() {

  // Initialize outputs
  pinMode(ledPin, OUTPUT);

  // Initialize inputs
  pinMode(pin1, INPUT); // seat 1
  pinMode(pin2, INPUT); // seat 2
  pinMode(pin3, INPUT); // seat 3
  pinMode(pin4, INPUT); // seat 4

  // WiFi setup
  WiFi.mode(WIFI_STA);
  WiFiMulti.addAP("Puppet Guest", "argon4949");

  // Debug setup
  Serial.begin(115200);
  for (uint8_t t = 4; t > 0; t--) {
    Serial.printf("[SETUP] WAIT %d...\n", t);
    Serial.flush();
    delay(1000);
  }
}

void firebaseState(String seatId, String occupied) {
  HTTPClient http;

  Serial.print("[HTTP] begin...\n");
  // configure traged server and url
  //http.begin("https://192.168.1.12/test.html", "7a 9c f4 db 40 d3 62 5a 6e 21 bc 5c cc 66 c8 3e a1 45 59 38"); //HTTPS
  http.begin("https://seatpeek.firebaseio.com/seats.json", "b8 4f 40 70 0c 63 90 e0 07 e8 7d bd b4 11 d0 4a ea 9c 90 f6"); //HTTPS

  Serial.print("[HTTP] Trying...\n");
  String json;
  json = "{ \"" + seatId + "\": { \"occupied\": " + occupied + ", \"type\": \"normal\" }}";
  // start connection and send HTTP header
  int httpCode = http.PATCH(json);

  // httpCode will be negative on error
  if (httpCode > 0) {
    // HTTP header has been sent and Server response header has been handled
    Serial.printf("[HTTP] GET... code: %d\n", httpCode);

    // file found at server
    if (httpCode == HTTP_CODE_OK) {
      String payload = http.getString();
      Serial.println(payload);
    }
  } else {
    Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
  }

  http.end();
}


void loop() {

  // wait for WiFi connection
  if ((WiFiMulti.run() == WL_CONNECTED)) {

    // read the pushbutton input pin:
    buttonState1 = digitalRead(pin1);
    buttonState2 = digitalRead(pin2);
    buttonState3 = digitalRead(pin3);
    buttonState4 = digitalRead(pin4);


    // compare the buttonState to its previous state
    if (buttonState1 != lastButtonState1) {
      // if the state has changed, increment the counter
      if (buttonState1 == HIGH) {
        // if the current state is HIGH then the button went from off to on:
        buttonPushCounter1++;
        Serial.println("on/bright/HIGH/empty/false");
        firebaseState("0000-00", "false");
      } else {
        // if the current state is LOW then the button went from on to off:
        Serial.println("off/dark/LOW/full/true");
        firebaseState("0000-00", "true");
      }
      // Delay a little bit to avoid bouncing
      delay(500);
    }

     // compare the buttonState to its previous state
    if (buttonState2 != lastButtonState2) {
      // if the state has changed, increment the counter
      if (buttonState2 == HIGH) {
        // if the current state is HIGH then the button went from off to on:
        buttonPushCounter2++;
        Serial.println("on/bright/HIGH/empty/false");
        firebaseState("0000-01", "false");
      } else {
        // if the current state is LOW then the button went from on to off:
        Serial.println("off/dark/LOW/full/true");
        firebaseState("0000-01", "true");
      }
      // Delay a little bit to avoid bouncing
      delay(500);
    }

     // compare the buttonState to its previous state
    if (buttonState3 != lastButtonState3) {
      // if the state has changed, increment the counter
      if (buttonState3 == HIGH) {
        // if the current state is HIGH then the button went from off to on:
        buttonPushCounter3++;
        Serial.println("on/bright/HIGH/empty/false");
        firebaseState("0000-02", "false");
      } else {
        // if the current state is LOW then the button went from on to off:
        Serial.println("off/dark/LOW/full/true");
        firebaseState("0000-02", "true");
      }
      // Delay a little bit to avoid bouncing
      delay(500);
    }

     // compare the buttonState to its previous state
    if (buttonState4 != lastButtonState4) {
      // if the state has changed, increment the counter
      if (buttonState4 == HIGH) {
        // if the current state is HIGH then the button went from off to on:
        buttonPushCounter4++;
        Serial.println("on/bright/HIGH/empty/false");
        firebaseState("0000-03", "false");
      } else {
        // if the current state is LOW then the button went from on to off:
        Serial.println("off/dark/LOW/full/true");
        firebaseState("0000-03", "true");
      }
      // Delay a little bit to avoid bouncing
      delay(500);
    }
    // save the current state as the last state, for next time through the loop
    lastButtonState1 = buttonState1;
    lastButtonState2 = buttonState2;
    lastButtonState3 = buttonState3;
    lastButtonState4 = buttonState4;

  }
}


